# bulma-rwx

This is a customized version of Bulma CSS framework.

## Documentation

If you are looking for documentation on how to use this framework, go to 
http://bulma.r-w-x.net/

## Developers

To generate the customized CSS file, use this command : 

``npm run build``

To change bulma dependency version

```
npm install bulma@0.4.2 --save-dev
npm run bulma-version
```

To change version and deploy, simply use this command:

``npm version X.Y.Z``
